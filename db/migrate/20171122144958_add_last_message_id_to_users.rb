class AddLastMessageIdToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :last_message_id, :string
  end
end
