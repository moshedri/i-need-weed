class AddMessageToManagers < ActiveRecord::Migration[5.1]
  def change
    add_column :managers, :last_message_id, :string
  end
end
