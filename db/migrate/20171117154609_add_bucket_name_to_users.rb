class AddBucketNameToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :bucket_name, :string
  end
end
