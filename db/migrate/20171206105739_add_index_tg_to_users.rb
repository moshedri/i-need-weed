class AddIndexTgToUsers < ActiveRecord::Migration[5.1]
  def change
    add_index :users, :uid
    add_index :users, :user_name
    add_index :users, :telegram_id
  end
end
