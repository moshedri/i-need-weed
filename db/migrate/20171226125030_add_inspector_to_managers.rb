class AddInspectorToManagers < ActiveRecord::Migration[5.1]
  def change
    add_column :managers, :inspector, :boolean, default: false
  end
end
