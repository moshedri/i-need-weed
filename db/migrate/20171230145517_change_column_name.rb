class ChangeColumnName < ActiveRecord::Migration[5.1]
  def change
    rename_column :users, :valid, :is_valid
  end
end
