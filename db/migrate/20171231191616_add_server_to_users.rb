class AddServerToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :server_ids, :string
  end
end
