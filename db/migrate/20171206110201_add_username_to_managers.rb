class AddUsernameToManagers < ActiveRecord::Migration[5.1]
  def change
    add_column :managers, :user_name, :string
    add_index :managers, :user_name
  end
end
