class CreateManagers < ActiveRecord::Migration[5.0]
  def change
    create_table :managers do |t|
      t.string :telegram_id
      t.string :first_name
      t.string :last_name
      t.boolean :is_admin , default: false 
      t.jsonb :bot_command_data, default: {}
      t.timestamps null: false
      t.timestamps
    end
    add_index :managers, :telegram_id  , unique: true
  end
end
