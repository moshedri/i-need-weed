class AddChatToManagers < ActiveRecord::Migration[5.1]
  def change
    add_column :managers, :last_chat_id, :string
  end
end
