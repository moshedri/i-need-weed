class AddTimeToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :complete_time, :datetime
    add_column :users, :active_time, :datetime
  end
end
