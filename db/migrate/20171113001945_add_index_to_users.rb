class AddIndexToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :tz, :string
    add_index :users, :tz
  end
end
