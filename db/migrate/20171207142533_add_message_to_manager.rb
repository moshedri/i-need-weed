class AddMessageToManager < ActiveRecord::Migration[5.1]
  def change
    add_column :managers, :message_mode, :boolean , default: false
  end
end
