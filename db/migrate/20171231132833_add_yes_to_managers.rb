class AddYesToManagers < ActiveRecord::Migration[5.1]
  def change
    add_column :managers, :daily_yes, :integer  , default: 0 
    add_column :managers, :daily_no, :integer, default: 0 
  end
end
