class AddFacebookPhotoToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :fb_url, :string
    add_column :users, :user_name, :string
    add_column :users, :is_active, :boolean , default: false
  end
end
