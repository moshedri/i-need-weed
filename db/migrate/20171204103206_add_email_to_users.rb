class AddEmailToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :fb_pp, :string
    add_column :users, :email, :string
  end
end
