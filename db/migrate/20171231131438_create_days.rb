class CreateDays < ActiveRecord::Migration[5.1]
  def change
    create_table :days do |t|
      t.integer :user
      t.integer :yes
      t.integer :no

      t.timestamps
    end
    add_index :days, [:user, :created_at]
  end
end
