class AddTokenForUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :photo_with_weed , :string
    add_column :users, :photo_with_card , :string
    add_column :users, :photo_of_card , :string
    add_column :users, :page_photo , :string
    add_column :users, :social_photo , :string
  end
end
