class AddErrorDataToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :manager_id, :string
    add_column :users, :error_date, :string
    add_column :users, :error_message, :string
  end
end
