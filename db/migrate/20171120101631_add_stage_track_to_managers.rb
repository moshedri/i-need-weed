class AddStageTrackToManagers < ActiveRecord::Migration[5.0]
  def change
    add_column :managers, :stage_track, :integer
    add_column :managers, :users_count, :integer
  end
end
