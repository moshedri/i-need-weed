class AddRequestsToManagers < ActiveRecord::Migration[5.1]
  def change
    add_column :managers, :requests_count, :integer , default: 0
  end
end
