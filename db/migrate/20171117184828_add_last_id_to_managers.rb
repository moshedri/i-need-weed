class AddLastIdToManagers < ActiveRecord::Migration[5.0]
  def change
    add_column :managers, :last_user_id, :string
    add_column :managers, :is_last_approved , :boolean
  end
end
