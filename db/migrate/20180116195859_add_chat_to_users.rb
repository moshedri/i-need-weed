class AddChatToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :last_chat_id, :string
    add_column :users, :last_worker, :string
  end
end
