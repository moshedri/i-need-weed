class AddValuesToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :values, :string
  end
end
