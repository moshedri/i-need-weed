class AddAdminToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :admin_id, :string
    add_column :users, :valid, :boolean , default: false
  end
end
