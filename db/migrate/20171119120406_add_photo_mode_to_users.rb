class AddPhotoModeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :is_photo_mode_on, :boolean ,default: false 
  end
end
