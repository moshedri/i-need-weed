# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180118180251) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "days", force: :cascade do |t|
    t.integer "user"
    t.integer "yes"
    t.integer "no"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user", "created_at"], name: "index_days_on_user_and_created_at"
  end

  create_table "managers", id: :serial, force: :cascade do |t|
    t.string "telegram_id"
    t.string "first_name"
    t.string "last_name"
    t.boolean "is_admin", default: false
    t.jsonb "bot_command_data", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "last_user_id"
    t.boolean "is_last_approved"
    t.integer "stage_track"
    t.integer "users_count"
    t.string "search"
    t.string "user_name"
    t.string "last_message_id"
    t.boolean "message_mode", default: false
    t.string "teseter_id", default: "493580589"
    t.boolean "test_mode", default: false
    t.integer "requests_count", default: 0
    t.string "group"
    t.integer "page", default: 1
    t.boolean "inspector", default: false
    t.integer "daily_yes", default: 0
    t.integer "daily_no", default: 0
    t.string "last_chat_id"
    t.index ["telegram_id"], name: "index_managers_on_telegram_id", unique: true
    t.index ["user_name"], name: "index_managers_on_user_name"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "telegram_id"
    t.string "first_name"
    t.string "last_name"
    t.jsonb "bot_command_data", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tz"
    t.string "phone_number"
    t.string "provider"
    t.string "uid"
    t.string "name"
    t.string "oauth_token"
    t.datetime "oauth_expires_at"
    t.string "remember_digest"
    t.string "fb_url"
    t.string "user_name"
    t.boolean "is_active", default: false
    t.string "bucket_name"
    t.string "manager_id"
    t.string "error_date"
    t.string "error_message"
    t.string "photo_with_weed"
    t.string "photo_with_card"
    t.string "photo_of_card"
    t.string "page_photo"
    t.string "social_photo"
    t.boolean "is_photo_mode_on", default: false
    t.string "last_message_id"
    t.integer "area"
    t.boolean "is_complete", default: false
    t.string "fb_pp"
    t.string "email"
    t.boolean "block", default: false
    t.boolean "valid_mode", default: false
    t.string "values"
    t.boolean "spam", default: false
    t.datetime "complete_time"
    t.datetime "active_time"
    t.string "admin_id"
    t.boolean "is_valid", default: false
    t.string "server_ids"
    t.string "zt_info"
    t.string "last_chat_id"
    t.string "last_worker"
    t.index ["phone_number"], name: "index_users_on_phone_number"
    t.index ["telegram_id"], name: "index_users_on_telegram_id"
    t.index ["tz"], name: "index_users_on_tz"
    t.index ["uid"], name: "index_users_on_uid"
    t.index ["user_name"], name: "index_users_on_user_name"
  end

end
