Rails.application.routes.draw do

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  get 'auth/:provider/callback', to: 'sessions#create'
  get'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'
 # resources :cands
  #resources :users
  #get "upload_photos/:id" , to: "users#upload_photos"
  get "welcome/:id" , to: "users#welcome"
  get "thank_you/:id" ,to: "users#thank_you"
  get "terms" ,to: "users#terms"
  get "block" , to: "users#block"
  get "find" ,to: "users#find"

 # post "upload_photos/:id" , to: "users#upload_photos"
  root :to => "users#error"
 # post "post_photos/:id"  , to: "users#post_photos"
 
  post '/webhooks/telegram_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' => 'webhooks#callback' #, as: :receive_webhooks
 #post "/webhooks/telegram_vbc43edbf1614a075954dvd4bfab34l1" => "webhooks#callback"
 
 # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
