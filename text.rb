רוך הבא לטלגראס Nana,

אנו שמחים שהחלטת להצטרף אלינו  כסוחר מאומת!
תהליך ההרשמה מורכב משלושה שלבים-

1. יצירת חשבון סוחר ע”י שליחת מספר ת.ז שלך ומספר הטוקמן עליו פתחת את חשבון הטלגרם.

2. שליחת תמונות אימות הכוללות: תעודת זהות, ספח וצילום מסך של פרופיל הפייסבוק.

3. שליחת וידאו אימות הכולל הצגת 50 ג(!) סחורה לצורכי אבטחה ואימות סחר

במידה ואחד השלבים לא הוגש כראוי, תקבל הודעה כאן הודעה איזו משימה עליך להשלים ומה הסיבה לדחיה!

על מנת לעבור את התהליך בהצלחה ובמהירות יש למלא אחר ההוראות במלואן,
בהצלחה!



1. אנא הזן את מספר תעודת הזהות שלך כולל ספרת ביקורת(9 ספרות ללא מקפים או רווח)

_________________________________

2. שלח תמונה עם תעודה מזהה כולל ספח  כאשר כל הפרטים נראים באופן ברור! 

- תעודת זהות כולל ספח!\רשיון\חוגר בלבד!

_________________________________

3. לחץ על הכפתור על מנת לשלוח לטלגראס את מספר הטוקמן עליו פתחת את היוזר בטלגרם.

_________________________________

4. אנא בחר איזור בו תרצה להתחיל לעבוד

מקלדת כפתורים: צפון | ירושלים | מרכז | דרום

_________________________________

5. שלח קישור דרך דפדפן גוגל לפרופיל הפייסבוק שלך

* אנא דאג שיהיה אפשר לצפות בפרופיל 
שלך - מקצר את התור 

-מה עושים אם אין לי פייסבוק? 
שלח תמונה של כרטיס קופת חולים+תלוש שכר ביחד באופן ברור

*שים לב פה יש בקלט או קישור(לרוב עם טעויות כמו שדיברנו) או תמונה


_________________________________


6. שלח וידאו קצר (עד 30 שניות) בו רואים אותך בבירור עם סחורה(לפחות 50 גרם, שקול/מחולק)
 אומר -
 ״השם שלך + אני רוצה לסחור בטלגראס + קוד ״

*קוד 9878

(הוידאו שלב אחרון כי מצריך הכי הרבה אמון מצד המשתמש באופן יחסי)
_________________________________


הפרטים התקבלו, נא המתן בסבלנות.
בקרוב ניצור איתך קשר!
במידה ויש תיקונים לעשות באחד משלבי האימות נפנה אליך דרך צ׳אט זה לקבלת התיקונים.