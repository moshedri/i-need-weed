json.extract! cand, :id, :created_at, :updated_at
json.url cand_url(cand, format: :json)
