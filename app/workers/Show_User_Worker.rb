class Show_User_Worker
    include Sidekiq::Worker

     sidekiq_options retry: false
    
   # sidekiq_options queue: :show_user

    def perform(manager_id)
#puts "----- starting show user -----"
        @user = Manager.find(manager_id)
        if @user.last_user_id  !=nil && @user.last_user_id != "0"
        @cand = User.find(@user.last_user_id)
     



        if @cand.is_active 
                
            @button='{
             "inline_keyboard": [
              [
      
               {
                   "text": " עדכונים ",
                  "callback_data": "/UpdateManagerFromSearch"
                 }
                  ,
                 {
                 "text": " חיפוש נוסף  ",
                  "callback_data": "/Search"
                }
                ,
                {
                "text": " חסום סוחר  ",
                 "callback_data": "/Block"
               }
               ,
               {
               "text": " הצג תמונות  ",
                "callback_data": "/ShowPhotos"
              }
            
            
               ] 
               ,
               [
               
                 {
                 "text": " שלח הודעת מערכת לסוחר  ",
                  "callback_data": "/MessageMode"
                }
                  ]
                  ,
                  [
                  
                    {
                    "text": " החזר לתור  ",
                     "callback_data": "/Unblock"
                   }
                     ]


               ]
                 }'


        elsif !@cand.is_complete && !@cand.block

            @button='{
              "inline_keyboard": [
               [
       
                {
                    "text": " עדכונים ",
                   "callback_data": "/UpdateManagerFromSearch"
                  }
                   ,
                  {
                  "text": " חיפוש נוסף  ",
                   "callback_data": "/Search"
                 }
                 ,
                 {
                 "text": " מחק משתמש  ",
                  "callback_data": "/DestroyThisLastUser"
                }
             
             
                ] 


                ,
                [
                
                  {
                  "text": " שלח הודעת מערכת לסוחר  ",
                   "callback_data": "/MessageMode"
                 }
                   ]
  
                ]
                  }'



        else
                if @cand.block
                  @button='{
                    "inline_keyboard": [
                      [
          
                        {
                          "text": " עדכונים ",
                          "callback_data": "/UpdateManagerFromSearch"
                           }
                             ,
                           {
                             "text": " חיפוש נוסף  ",
                           "callback_data": "/Search"
                             }
                
                
                             ] 
                             ,
                             [
                             
                               {
                               "text": " בטל חסימה  ",
                                "callback_data": "/Unblock"
                              }
                                ]
  
     
                              ]
                                }'
                else


                                 if @user.inspector
                                    @button='{
                                      "inline_keyboard": [
                                     [
      
                                       {
                                     "text": " עדכונים ",
                                      "callback_data": "/UpdateManagerFromSearch"
                                      }
                                     ,
                                         {
                                     "text": " חיפוש נוסף  ",
                                     "callback_data": "/Search"
                                         }
            
            
                                              ]
                                             ,
                                      [
                                     {
                                       "text": " החל אימות  ",
                                       "callback_data": "/FromSearch"
                                       } 
                                         ,
                                      {
                                         "text": " חסום סוחר  ",
                                           "callback_data": "/Block"
                                       }
                                       ]
                                          ,
                                         [
                            
                                        {
                                     "text": " שלח הודעת מערכת לסוחר  ",
                                    "callback_data": "/MessageMode"
                                        }
                                       ]
 
                                         ]
                                        }'

                                else

                                       ## not inspector
                                      @button='{
                                     "inline_keyboard": [
                                     [
                  
                                      {
                                    "text": " עדכונים ",
                                  "callback_data": "/UpdateManagerFromSearch"
                                   }
                                     ,
                                   {
                                     "text": " חיפוש נוסף  ",
                                   "callback_data": "/Search"
                                     }
                        
                        
                                     ]
                                      ,
                                     [
                                 
                                       {
                                       "text": " חסום סוחר  ",
                                        "callback_data": "/Block"
                                      }
                                        ]
                                        ,
                                        [
                                        
                                          {
                                          "text": " שלח הודעת מערכת לסוחר  ",
                                           "callback_data": "/MessageMode"
                                         }
                                           ]
             
                                      ]
                                        }'
      
                        
                                end

                        end          
              end

 
      
      
      
          

            else
            @text = "לא נמצא משתמש כזה במערכת"

            
            end

if @button.nil?

  @button='{
    "inline_keyboard": [
      [
  
        {
          "text": " קבל עדכונים ",
          "callback_data": "/UpdateManager"
        }
        ,
        {
          "text": " חיפוש ",
          "callback_data": "/Search"
        }
      ]
  
    ]
  }'
  end  

 # puts "----- starting show user message -----"
            @user.update_attributes!(search: nil , bot_command_data: {})
            @user.set_next_bot_command('BotCommand::UpdateManager')
           # puts "text:"+@text.to_s
           # puts"button:"+@button.to_s
        #   @user.send_message_with_html(@text , @button)   
        
        if @text.nil?
        @text = @user.user_view(@user.last_user_id)
        end
      
             @user.send_message_with_html(@text, @button)
      


          #  puts "----- end of show user -----"

    end
  end