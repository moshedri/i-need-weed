class Ask_For_Tz_Worker
    include Sidekiq::Worker

     sidekiq_options retry: false
    
    def perform(id)

        @user = User.find(id)

        
        @getUserTzMessage = "
        אנא הזן את מספר תעודת הזהות שלך כולל ספרת ביקורת
        ( 9 ספרות ללא מקפים או רווח )
        "
        o = [('a'..'z'), ('A'..'Z')].map(&:to_a).flatten
        @token  = (0...50).map { o[rand(o.length)] }.join
        @user.update_attributes!(remember_digest: @token ,bot_command_data: {})
        @user.set_next_bot_command('BotCommand::AccomplishTutorial')
        @user.send_message( @getUserTzMessage  )



    end
  end