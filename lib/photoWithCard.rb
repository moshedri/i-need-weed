require 'bot_command'
module BotCommand
    class PhotoWithCard < Base
      def should_start?
        return true
      end
  
        def start
        
   
         text = " עכשיו צילום ברור של התעודה לבד "
  
         error =" התמונה לא נשלחה בהצלחה אנא נסה שנית "
         if user.is_photo_mode_on   
          user.update_attributes!(bot_command_data: {} , is_photo_mode_on: false)
         user.set_next_bot_command('BotCommand::PhotoOfCard')
          send_message(text) 
         else 
           send_message(error)
         end
         end
       end
     end